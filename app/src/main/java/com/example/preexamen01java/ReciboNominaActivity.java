package com.example.preexamen01java;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

public class ReciboNominaActivity extends AppCompatActivity {
    // Declaración de objetos
    private TextView lblUsuario;
    private EditText txtNumRecibo;
    private EditText txtNombre;
    private EditText txtHorasTrabNormales;
    private EditText txtHorasTrabExtras;
    private RadioButton rbAuxiliar;
    private RadioButton rbAlbañil;
    private RadioButton rbIngObra;

    private EditText txtSubtotal;
    private EditText txtImpuesto;
    private EditText txtTotalPagar;

    private Button btnCalcular;
    private Button btnLimpiar;
    private Button btnRegresar;

    private ReciboNomina recibo;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recibo);
        iniciarComponentes();

        // Obtener los datos del MainActivity
        Bundle datos = getIntent().getExtras();
        String nombre = datos.getString("nombre");
        lblUsuario.setText(nombre);

        // Evento click de los botones
        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calcular();
            }
        });
        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                limpiar();
            }
        });
        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                regresar();
            }
        });
    }

    private void iniciarComponentes() {
        // Etiqueta
        lblUsuario = findViewById(R.id.lblUsuario);

        // Cajas de texto
        txtNumRecibo = findViewById(R.id.txtNumRecibo);
        txtNombre = findViewById(R.id.txtNombre);
        txtHorasTrabNormales = findViewById(R.id.txtHorasTrabNormales);
        txtHorasTrabExtras = findViewById(R.id.txtHorasTrabExtras);
        txtSubtotal = findViewById(R.id.txtSubtotal);
        txtImpuesto = findViewById(R.id.txtImpuesto);
        txtTotalPagar = findViewById(R.id.txtTotalPagar);

        // Radio Buttons
        rbAuxiliar = findViewById(R.id.rbAuxiliar);
        rbAlbañil = findViewById(R.id.rbAlbañil);
        rbIngObra = findViewById(R.id.rbIngObra);

        // Botones
        btnCalcular = findViewById(R.id.btnCalcular);
        btnLimpiar = findViewById(R.id.btnLimpiar);
        btnRegresar = findViewById(R.id.btnRegresar);

        // Objeto ReciboNomina
        recibo = new ReciboNomina(0,"",0,0,0,0);

        deshabilitarTextos();
    }

    private void calcular() {
        int puesto = 0;
        float horasNormales = 0;
        float horasExtras = 0;
        float subtotal = 0;
        float impuesto = 0;
        float total = 0;

        if(rbAuxiliar.isChecked()) {
            puesto = 1;
        }else if(rbAlbañil.isChecked()) {
            puesto = 2;
        }else if(rbIngObra.isChecked()) {
            puesto = 3;
        }else if(puesto == 0) {
            Toast.makeText(this, "Por favor, seleccione un puesto", Toast.LENGTH_SHORT).show();
        }

        if (txtNumRecibo.getText().toString().isEmpty()) {
            Toast.makeText(this, "Por favor, ingrese un número de recibo", Toast.LENGTH_SHORT).show();
        } else if (txtNombre.getText().toString().isEmpty()) {
            Toast.makeText(this, "Por favor, ingrese un nombre", Toast.LENGTH_SHORT).show();
        } else if (txtHorasTrabNormales.getText().toString().isEmpty() || txtHorasTrabExtras.getText().toString().isEmpty()) {
            if (txtHorasTrabNormales.getText().toString().isEmpty()) {
                Toast.makeText(this, "Ingrese una cantidad de horas trabajadas", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, "Ingrese una cantidad de horas extras", Toast.LENGTH_SHORT).show();
            }
        }

        if (!txtHorasTrabNormales.getText().toString().isEmpty() && !txtHorasTrabExtras.getText().toString().isEmpty() && puesto != 0){
            horasNormales = Float.parseFloat(txtHorasTrabNormales.getText().toString());
            horasExtras = Float.parseFloat(txtHorasTrabExtras.getText().toString());

            subtotal = recibo.calcularSubtotal(puesto, horasNormales, horasExtras);
            impuesto = recibo.calcularImpuesto(puesto, horasNormales, horasExtras);
            total = recibo.calcularTotal(puesto, horasNormales, horasExtras);

            // Mostrar el resultado de los cálculos
            txtSubtotal.setText("" + subtotal);
            txtImpuesto.setText("" + impuesto);
            txtTotalPagar.setText("" + total);
        }
    }

    private void limpiar() {
        txtNumRecibo.setText("");
        txtNombre.setText("");
        txtHorasTrabNormales.setText("");
        txtHorasTrabExtras.setText("");
        txtSubtotal.setText("");
        txtImpuesto.setText("");
        txtTotalPagar.setText("");
    }

    private void regresar() {
        AlertDialog.Builder confirmar = new AlertDialog.Builder(this);
        confirmar.setTitle("Recibo de Nómina");
        confirmar.setMessage("Regresar al MainActivity");
        confirmar.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        confirmar.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //No hace nada
            }
        });
        confirmar.show();
    }

    private void deshabilitarTextos() {
        txtSubtotal.setFocusable(false);
        txtImpuesto.setFocusable(false);
        txtTotalPagar.setFocusable(false);
    }
}