package com.example.preexamen01java;

public class ReciboNomina {
    // Declaración de varibles privadas en la clase
    private int numRecibo;
    private String nombre;
    private float horasTrabNormal;
    private float horasTrabExtras;
    private int puesto;
    private float impuestoPorc;

    // Constructor de parámetros para variables públicas
    public ReciboNomina(int numRecibo, String nombre, float horasTrabNormal, float horasTrabExtras, int puesto, float impuestoPorc) {
        this.numRecibo = numRecibo;
        this.nombre = nombre;
        this.horasTrabNormal = horasTrabNormal;
        this.horasTrabExtras = horasTrabExtras;
        this.puesto = puesto;
        this.impuestoPorc = impuestoPorc;
    }

    // Set & Get de cada variable
    public void setNumRecibo(int numRecibo) {
        this.numRecibo = numRecibo;
    }

    public int getNumRecibo() {
        return numRecibo;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNombre() {
        return nombre;
    }

    public void setHorasTrabNormal(float horasTrabNormal) {
        this.horasTrabNormal = horasTrabNormal;
    }

    public float getHorasTrabNormal() {
        return horasTrabNormal;
    }

    public void setHorasTrabExtras(float horasTrabExtras) {
        this.horasTrabExtras = horasTrabExtras;
    }

    public float getHorasTrabExtras() {
        return horasTrabExtras;
    }

    public void setPuesto(int puesto) {
        this.puesto = puesto;
    }

    public int getPuesto() {
        return puesto;
    }

    public void setImpuestoPorc(float impuestoPorc) {
        this.impuestoPorc = impuestoPorc;
    }

    public float getImpuestoPorc() {
        return impuestoPorc;
    }

    // Cálculo subtotal
    public float calcularSubtotal(int puesto, float horasTrabNormal, float horasTrabExtras){
        float pagoBase = 200;
        float subtotal = 0;
        // Cálculo de pago base según el puesto
        if(puesto == 1){
            pagoBase = (float) (pagoBase + (pagoBase * 0.20));
        } else if (puesto == 2) {
            pagoBase = (float) (pagoBase + (pagoBase * 0.50));
        } else if (puesto == 3) {
            pagoBase = (float) (pagoBase + (pagoBase * 1.00));
        } else {
            System.out.println("Calculo no valido");
        }
        // Cálculo del subtotal (se paga al doble)
        subtotal = ((pagoBase * horasTrabNormal) + (horasTrabExtras * pagoBase * 2));

        return subtotal;
    }

    // Cálculo impuesto
    public float calcularImpuesto(int puesto, float horasTrabNormal, float horasTrabExtras) {
        float impuesto = (float) (calcularSubtotal(puesto, horasTrabNormal, horasTrabExtras) * 0.16);
        return impuesto;
    }

    // Cálculo total
    public float calcularTotal(int puesto, float horasTrabNormal, float horasTrabExtras) {
        float subtotal = (float) (calcularSubtotal(puesto, horasTrabNormal, horasTrabExtras));
        float impuesto = (float) (calcularImpuesto(puesto, horasTrabNormal, horasTrabExtras));
        float totalPagar = subtotal - impuesto;
        return totalPagar;
    }
}
