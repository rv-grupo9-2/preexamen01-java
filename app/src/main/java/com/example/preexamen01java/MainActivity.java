package com.example.preexamen01java;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private TextView lblNombreTrabajador;
    private EditText txtNombreTrabajador;

    private Button btnEntrar;
    private Button btnSalir;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        iniciarComponentes();

        btnEntrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                entrar();
            }
        });
        btnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                salir();
            }
        });
    }

    private void iniciarComponentes() {
        lblNombreTrabajador = findViewById(R.id.lblNombreTrabajador);
        txtNombreTrabajador = findViewById(R.id.txtNombreTrabajador);
        btnEntrar = findViewById(R.id.btnEntrar);
        btnSalir = findViewById(R.id.btnSalir);
    }

    private void entrar() {
        String strNombre;

        strNombre = getApplicationContext().getResources().getString(R.string.nombre);

        if(strNombre.toString().equals(txtNombreTrabajador.getText().toString().trim())) {
            // Hacer el paquete para enviar la información
            Bundle bundle = new Bundle();
            bundle.putString("nombre", txtNombreTrabajador.getText().toString());

            // Crear el Intent para llamar a otra actividad
            Intent intent = new Intent(MainActivity.this, ReciboNominaActivity.class);
            intent.putExtras(bundle);

            // Iniciar la actividad esperando respuesta o no
            startActivity(intent);
        } else {
            Toast.makeText(this.getApplicationContext(), "El nombre de usuario no es válido",
                    Toast.LENGTH_SHORT).show();
        }
    }

    private void salir() {
        finish();
    }
}